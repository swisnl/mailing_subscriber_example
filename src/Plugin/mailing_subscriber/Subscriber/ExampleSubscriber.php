<?php


namespace Drupal\mailing_subscriber_example\Plugin\mailing_subscriber\Subscriber;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailing_subscriber\Annotation\Subscriber;
use Drupal\mailing_subscriber\Plugin\mailing_subscriber\Subscriber\SubscriberBase;

/**
 * Defines an example plugin.
 *
 * @Subscriber(
 *   id = "example",
 *   label = @Translation("Example"),
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Subscriber"),
 * )
 */
class ExampleSubscriber extends SubscriberBase {

  /**
   * Get the label.
   *
   * @return string
   *   The label.
   */
  public function getLabel() {
    return t('Example type');
  }

  public function defaultConfiguration() {
    return [
        'api_key' => '',
        'location_key' => ''
      ] + parent::defaultConfiguration();
  }


  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Example API key'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];
    $form['location_key'] = [
      '#type' => 'textfield',
      '#title' => t('Example location key'),
      '#default_value' => $this->configuration['location_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    //Validate form
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['api_key'] = $form_state->getValue('api_key');
    $this->configuration['location_key'] = $form_state->getValue('location_key');
  }

  /**
   * Subscribe the given email address.
   *
   * @param string $email
   */
  public function subscribe($email) {
    //Subscribe implementation
  }

}
